import { Injectable } from '@angular/core';
import { Post } from '../models/post.model';
import { Subject } from 'rxjs';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  posts: Post[] = [];
  postsSubject = new Subject<Post[]>();
  isReverse = true;

  constructor() {
    this.getPosts();
  }

  emitPosts() {
    this.postsSubject.next(this.posts);
  }

  getPosts() {
    firebase.database().ref('/posts')
      .orderByChild('date')
      .on('value', (data: DataSnapshot) => {
        this.posts = data.val() ? data.val() : [];
        if (this.isReverse) {
          this.posts.reverse();
        }
        this.emitPosts();
      });
    }

  savePosts() {
    firebase.database().ref('/posts').set(this.posts);
  }

  saveSinglePost(post: Post) {
    if (this.isReverse) {
      this.posts.reverse();
    }
    firebase.database().ref('/posts/' + this.findIndexInPosts(post)).set(post);
  }

  createNewPost(post: Post) {
    if (this.isReverse) {
      this.posts.unshift(post);
    } else {
      this.posts.push(post);
    }
    this.saveSinglePost(post);
    this.emitPosts();
  }

  removePost(post: Post) {
    if (this.isReverse) {
      this.posts.reverse();
    }
    const postIndexToRemove = this.findIndexInPosts(post);
    this.posts.splice(postIndexToRemove, 1);
    this.savePosts();
    this.emitPosts();
  }

  findIndexInPosts(post: Post) {
    const postIndex = this.posts.findIndex(
      (postEl) => {
        if (postEl === post) {
          return true;
        }
      }
    );
    return postIndex;
  }

  getIsReverse() {
    return this.isReverse;
  }

  switchReverse() {
    if (this.isReverse) {
      this.isReverse = false;
    } else {
      this.isReverse = true;
    }
    this.posts.reverse();
    this.emitPosts();
    return this.isReverse;
  }

  resetBdd() {
    const defaultPosts = [
      { titre: 'Mon premier post', contenu: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean dapibus metus nec cursus convallis. Cras a eros semper, accumsan risus non, cursus orci. In hac habitasse platea dictumst. Nunc nec pharetra nulla. Suspendisse vitae leo in diam malesuada faucibus id quis libero. Quisque tincidunt dolor augue, et bibendum turpis sodales sit amet. Curabitur sodales cursus augue ac aliquam. Maecenas sollicitudin vehicula est, vel volutpat mi lobortis et. Pellentesque vel cursus nulla, vel vulputate urna. In dignissim eros in velit venenatis, at sollicitudin erat ultrices.', date: 1567980832131, loveIts: 1 },
      { titre: 'Mon deuxième post', contenu: 'Sed quis sagittis est. Donec ac sapien mauris. Praesent convallis diam in libero feugiat luctus. Sed nec mattis augue. Proin dui eros, interdum et placerat vitae, egestas aliquam urna. Pellentesque hendrerit facilisis mauris, sed dapibus enim vehicula sit amet. Mauris id bibendum arcu, non pulvinar urna.', date: 1567981837220, loveIts: -1 },
      { titre: 'Mon troisième post', contenu: 'Nunc efficitur velit dui, vel molestie mauris lacinia eget. Curabitur malesuada tristique maximus. Quisque imperdiet, libero id euismod porta, risus lorem venenatis nisl, non molestie purus ipsum non urna. Integer dignissim, leo sed consequat imperdiet, est justo mollis neque, in pulvinar erat risus eu ante. Aenean hendrerit velit nibh, id feugiat nisi finibus eget. Integer vel feugiat nulla, eu volutpat nunc. Aenean porta auctor turpis, id aliquet orci laoreet ut. Suspendisse semper ornare aliquam. Duis et turpis turpis. Mauris eleifend maximus enim id faucibus. Curabitur at pulvinar urna. Sed vel feugiat ligula. Vivamus a nisl elit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;', date: 1567981950294, loveIts: 0 }
    ];
    this.posts = [];
    this.savePosts();
    this.emitPosts();
    for (const post of defaultPosts) {
      const newPost = new Post(post.titre, post.contenu, post.date, post.loveIts);
      this.posts.push(post);
    }
    this.savePosts();
    this.emitPosts();
  }
}
