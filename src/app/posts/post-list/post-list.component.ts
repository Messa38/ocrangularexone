import { Component, OnInit, OnDestroy } from '@angular/core';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/post.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

  posts: Post[];
  postsSubscription: Subscription;
  isReverse = false;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.postsSubscription = this.postsService.postsSubject.subscribe(
      (posts: Post[]) => {
        this.posts = posts;
      }
    );
    this.postsService.emitPosts();
    this.isReverse = this.postsService.getIsReverse();
  }

  ngOnDestroy() {
    this.postsSubscription.unsubscribe();
  }

  onSortAsc() {
    if (this.isReverse) {
      this.isReverse = this.postsService.switchReverse();
    }
  }
  onSortDesc() {
    if (!this.isReverse) {
      this.isReverse = this.postsService.switchReverse();
    }
  }
}
