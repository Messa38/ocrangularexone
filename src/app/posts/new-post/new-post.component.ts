import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostsService } from 'src/app/services/posts.service';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {

  postForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private postsService: PostsService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      titre: ['', Validators.required],
      contenu: ['', Validators.required]
    });
  }

  onSavePost() {
    const titre = this.postForm.get('titre').value;
    const contenu = this.postForm.get('contenu').value;
    const pdate = new Date().getTime();
    const loveIts = 0;
    const newPost = new Post(titre, contenu, pdate, loveIts);
    this.postsService.createNewPost(newPost);
    this.router.navigate(['/posts']);
  }

  onCancel() {
    this.router.navigate(['/posts']);
  }
}
