import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from 'src/app/services/posts.service';
import { Post } from 'src/app/models/post.model';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() post: Post;

  constructor(private postsService: PostsService) { }

  ngOnInit() {
  }

  onCounterUp() {
    this.post.loveIts++;
    this.postsService.saveSinglePost(this.post);
    this.postsService.emitPosts();
  }
  onCounterDown() {
    this.post.loveIts--;
    this.postsService.saveSinglePost(this.post);
    this.postsService.emitPosts();
  }

  onDeletePost(post: Post) {
    if (confirm('Êtes vous sûr de vouloir supprimer ce post ?')) {
      this.postsService.removePost(post);
    }
  }
}
