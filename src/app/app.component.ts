import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor() {
    const config = {
      apiKey: 'AIzaSyA16Q-Vs4KaDIBy4UWbye21DQB9sVNIgDU',
      authDomain: 'mon-blog-f5535.firebaseapp.com',
      databaseURL: 'https://mon-blog-f5535.firebaseio.com',
      projectId: 'mon-blog-f5535',
      storageBucket: '',
      messagingSenderId: '903357936995',
      appId: '1:903357936995:web:ae2ed4d3e5916212ec8f56'
    };
    firebase.initializeApp(config);
  }
}
