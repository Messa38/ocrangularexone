export class Post {
  constructor(public titre: string,
              public contenu: string,
              public date: number,
              public loveIts: number) {}
}
